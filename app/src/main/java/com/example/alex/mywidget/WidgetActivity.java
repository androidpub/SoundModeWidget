package com.example.alex.mywidget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RemoteViews;
import android.widget.Toast;

public class WidgetActivity extends AppWidgetProvider {

    AudioManager audioManager;


    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

        for (int i : appWidgetIds) {
            updateWidget(context, appWidgetManager, i);
        }




    }

    static void updateWidget(Context context, AppWidgetManager appWidgetManager, int widgetID) {
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.activity_widget);

//vibro mode
        Intent vibroIntent = new Intent(context, WidgetActivity.class);

        vibroIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,widgetID);
        vibroIntent.setAction("Vibro");

        PendingIntent pIntent = PendingIntent.getActivity(context, widgetID, vibroIntent, 0);
        pIntent=PendingIntent.getBroadcast(context, widgetID,vibroIntent,0);
        views.setOnClickPendingIntent(R.id.btn_vibro, pIntent);

//normal mode
        Intent soundIntent = new Intent(context, WidgetActivity.class);

        soundIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,widgetID);
        soundIntent.setAction("Sound");

        pIntent = PendingIntent.getActivity(context, widgetID, soundIntent, 0);
        pIntent=PendingIntent.getBroadcast(context, widgetID,soundIntent,0);
        views.setOnClickPendingIntent(R.id.btn_sound, pIntent);





        appWidgetManager.updateAppWidget(widgetID, views);






    }




    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);

        String curAction = intent.getAction();
        String vibroAction="Vibro";
        String soundAction="Sound";


        audioManager=(AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
    if(curAction.equals(vibroAction)){

        audioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
        Toast.makeText(context,"Vibrate",Toast.LENGTH_LONG).show();
        curAction=soundAction;
        }else{
        audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
        Toast.makeText(context,"Normal",Toast.LENGTH_LONG).show();
        curAction=vibroAction;
    }








    }
}
